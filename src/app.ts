// =================================
//
//	Media Storage Server
//	Copyright Grebennikov Evgenii.
//
// =================================

import * as express from "express";
import * as errorHandler from "errorhandler";
import * as bodyParser from "body-parser";
import * as path from "path";
import * as http from "http";
import * as WebSocket from "ws";
import * as fs from "fs";

import { logger } from "./logger";
import { Settings } from "./settings";
import { mkdirp, checkDirectory, getStatsList, createHLSDescriptor, concatVideo } from "./utils/FSUtil";
import * as dateFormat from "dateformat";
import * as uuidv1 from "uuid/v1";

export class App {

    public static PUBLIC: string = "public";
    public static UPLOADS: string = "uploads";

    private _expressApp: express.Application;
    public get expressApp(): express.Application {
        return this._expressApp;
    }

    private _httpService: http.Server;
    public get httpService(): http.Server {
        return this._httpService;
    }

    private _publicDir: string;
    get publicDir(): string { return this._publicDir; }

    static instance: App;

    constructor() {
        App.instance = this;

        process.on('uncaughtException', (e) => {
            logger.error(e.message);
        });

        Settings.read((err) => {
            if (err) return logger.error("Error reading \"config.json\"");
        });

        this.initializeEnv();
    }

    private initializeEnv(): void {
        this._publicDir = `${__dirname}${path.sep}${App.PUBLIC}`;
        //this._publicDir = `${path.dirname(process.execPath)}${path.sep}${App.PUBLIC}`;

        mkdirp(`${this._publicDir}`)
            .then(() => {
                this.runServer();
                console.log('runServer');
            })
            .catch((reason: NodeJS.ErrnoException) => {
                if (reason) logger.error(`Can not create public directory. ${reason.message}`);
            })
    }

    private runServer(): void {
        this._expressApp = express();

        this._expressApp.set("port", Settings.server.port);

        this._expressApp.use(errorHandler());
        this._expressApp.use(bodyParser.json()); // to support JSON-encoded bodies
        this._expressApp.use(bodyParser.urlencoded({ // to support URL-encoded bodies
            extended: true
        }));

        this._httpService = http.createServer(this._expressApp);

        this._httpService.addListener("close", () => {
            logger.error(`Http server is stopped.`);
        });

        this._httpService.addListener("error", (err: Error) => {
            logger.error(`Http server error. ${err}`);
        });

        this._httpService.listen(this._expressApp.get("port") || 8999, () => {
            logger.info(`Server running at http://localhost:${this._expressApp.get("port")} in ${this._expressApp.get("env")} mode`);
            logger.info("Press CTRL-C for stoping server\n");
            //this.run();
        });

        // Добавление разрешенных заголовков
        this._expressApp.use(function (req, res, next) {
            res.header("Access-Control-Allow-Origin", Settings.centralServer.address);
            res.header("Access-Control-Allow-Methods", "GET,POST,PUT,DELETE,OPTIONS");
            res.header("Access-Control-Allow-Headers", "Origin, Content-Type, Accept, Cache-control, Content-length");
            res.setHeader("Cache-control", "no-cache");
            if (req.method === "OPTIONS") {
                res.end();
            } else {
                next();
            }
        });

        this._expressApp.use(express.static(this._publicDir));

        this.configurateRoutes();
    }

    private configurateRoutes(): void {

        this._expressApp.post("/api/downloadvideo", (req: express.Request, res: express.Response) => {
            this.downloadVideo(req, res);
        });

        // Формирует hls-поток из заданной даты и времени
        this._expressApp.post("/api/getdatesequence", (req: express.Request, res: express.Response) => {
            this.createHlsWithDateTime(req, res);
        });

        // Загрузка файлов
        this._expressApp.put("*", (req: express.Request, res: express.Response) => {
            this.writeFile(req, res);
        })

        // Удаление файлов
        this._expressApp.delete("*", (req: express.Request, res: express.Response) => {
            this.deleteFiles(req, res);
        });
    }

    /**
     * Запись файлов в медиа-хранилеще
     */
    private writeFile(req: express.Request, res: express.Response): void {
        let p = req.path;

        // директория вывода архива
        const date = dateFormat(new Date(), "yyyy_mm_dd");

        // на m3u8 правила __f не действуют
        if (p.indexOf('.m3u8') !== -1) p = p.replace("__f", "");

        // применение правила __f
        p = p.replace("__f", date);

        const filePath: string = path.normalize(`${this._publicDir}${p}`);
        let d = filePath.substring(0, filePath.lastIndexOf(path.sep) + 1);

        logger.info(p);

        if (res.statusCode !== 200) {
            logger.warn('File couldn\'t be retrieved');
            return;
        }
        const isDescriptor = p.indexOf("m3u8") !== -1;
        let ba: any[] = [];
        let isDirCreated: boolean = false;
        let isEnd: boolean = false;
        let stream: fs.WriteStream;

        const deleteTmp = (path: string) => {
            fs.unlink(`${filePath}.tmp`, (error) => {
                if (error) logger.error(`#delete temp file failed. ${error}`);
            })
        };

        const write = () => {
            if (isDirCreated) {
                if (ba) {
                    for (const chunk of ba) {
                        if (isDescriptor) {
                            // Нормализация дескриптора
                            let descriptor = String(chunk);
                            while (descriptor.indexOf("__f") !== -1) {
                                descriptor = descriptor.replace("__f", date);
                            }
                            stream.write(descriptor, 'utf-8');
                        }
                        else
                            stream.write(chunk, 'binary');
                    }
                    ba = [];
                }
                if (isEnd) {
                    stream.end(() => {
                        fs.rename(`${filePath}.tmp`, filePath, (err: NodeJS.ErrnoException) => {
                            if (err) {
                                logger.error(`[PUT]::#renameFile error. ${err}`);
                                res.status(401).send(`Can not rename temp file. (${err})`);
                                deleteTmp(`${filePath}.tmp`);
                            } else {
                                res.send('ok');
                                ba = null;
                            }
                        })
                    });
                }
            }
        };

        mkdirp(d)
            .then(() => {
                isDirCreated = true;
                stream = fs.createWriteStream(`${filePath}.tmp`);
                write();
            })
            .catch((reason: NodeJS.ErrnoException) => {
                logger.error(`[PUT]::#mkDir error. ${reason.message}`);
                res.status(401).send(`Can not create output directory (${filePath})`);
                stream.end(() => {
                    deleteTmp(`${filePath}.tmp`);
                });
            });

        req.on('data', (chunk) => {
            if (ba) ba.push(chunk);
            if (isDirCreated) write();
        });
        req.on('end', () => {
            isEnd = true;
            write();
        });
        req.on('error', (err: Error) => {
            logger.error(`[PUT]::#writeChunk error. ${err.message}`);
            res.status(401).end(err.message);
            stream.end(() => {
                deleteTmp(`${filePath}.tmp`);
            });
        });
    }

    /**
     * Удаление файлов из медиа-хранилища
     */
    private deleteFiles(req: express.Request, res: express.Response): void {
        setTimeout(() => {
            let p = req.path;
            p = p.replace("__f", dateFormat(new Date(), "yyyy_mm_dd"));
            const filePath: string = path.normalize(`${this._publicDir}${p}`);

            fs.exists(filePath, (exists: boolean) => {
                if (exists) fs.unlink(filePath, (err) => {
                    if (err) logger.error(`[DEL]::Error. ${err.message}`);
                    //else res.send('ok');
                });
            })
        }, 5000);
        res.send('ok');
    }

    /**
     * Создание HLS-потока. Возвращается ссылка на манифест.
     */
    private createHlsWithDateTime(req: express.Request, res: express.Response): void {
        const date: string = req.body.date;
        const uuid: string = req.body.uuid;
        const range: Array<number> = req.body.range;
        const cam: string = req.body.cam;
        const address: string = req.body.address;
        const quality: string = req.body.quality;

        if (!date) {
            logger.error("#getdatesequence:: Requested date is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested date is empty.", code: 548 }, null));
        }
        if (!uuid) {
            logger.error("#getdatesequence:: Requested uuid is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested uuid is empty.", code: 549 }, null));
        }
        if (!cam) {
            logger.error("#getdatesequence:: Requested cam is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested cam is empty.", code: 550 }, null));
        }
        if (!quality) {
            logger.error("#getdatesequence:: Requested quality is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested quality is empty.", code: 551 }, null));
        }
        if (!address) {
            logger.error("#getdatesequence:: Requested address is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested address is empty.", code: 552 }, null));
        }

        const hlsPath: string = this.getSequencePath(req.body, cam, date, quality);
        const descriptorDir = `${uuid}${path.sep}${cam}${path.sep}${quality}${path.sep}requests`;
        const dir = `${this._publicDir}${path.sep}${uuid}${path.sep}${cam}${path.sep}${quality}${path.sep}${date}`;

        checkDirectory(dir)
            .then(() => {
                getStatsList(dir, { range: range })
                    .then((statsList) => {
                        if (statsList && statsList.length > 0) {
                            createHLSDescriptor(statsList, address, hlsPath, this._publicDir, descriptorDir)
                                .then((descriptor) => {
                                    res.send(this.createResponseData<IGetDateSequence>(null, {
                                        uuid: uuid,
                                        date: date,
                                        time: descriptor.range,
                                        descriptor: `${descriptorDir}${path.sep}${descriptor.name}`
                                    }));
                                })
                                .catch((reason) => {
                                    res.send(this.createResponseData<IGetDateSequence>({ message: `CreateHLSDescriptor error. ${reason}`, code: 720 }, null));
                                });
                        } else {
                            res.send(this.createResponseData<IGetDateSequence>(null, {
                                uuid: uuid,
                                date: date,
                                time: null,
                                descriptor: null
                            }
                            ));
                        }
                    })
                    .catch((reason) => {
                        res.send(this.createResponseData<IGetDateSequence>({ message: `GetStatsList error. ${reason}`, code: 710 }, null));
                    })
            })
            .catch((reason) => {
                res.send(this.createResponseData<IGetDateSequence>({ message: `Date (${date}) for {${uuid}} not found.`, code: 700 }, null));
            })
    }

    /**
     * Процесс формирование mp4 видеофайла и формирование ссылки для скачивания.
     */
    private downloadVideo(req: express.Request, res: express.Response): void {
        const date: string = req.body.date;
        const uuid: string = req.body.uuid;
        const range: Array<number> = req.body.range;
        const cam: string = req.body.cam;
        const address: string = req.body.address;
        const quality: string = req.body.quality;

        if (!date) {
            logger.error("#getdatesequence:: Requested date is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested date is empty.", code: 548 }, null));
        }
        if (!uuid) {
            logger.error("#getdatesequence:: Requested uuid is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested uuid is empty.", code: 549 }, null));
        }
        if (!cam) {
            logger.error("#getdatesequence:: Requested cam is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested cam is empty.", code: 550 }, null));
        }
        if (!quality) {
            logger.error("#getdatesequence:: Requested quality is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested quality is empty.", code: 551 }, null));
        }
        if (!address) {
            logger.error("#getdatesequence:: Requested address is empty.")
            res.send(this.createResponseData<IGetDateSequence>({ message: "#getdatesequence:: Requested address is empty.", code: 552 }, null));
        }

        //const hlsPath: string = this.getSequencePath(req.body, cam, date, quality);
        const descriptorDir = `${uuid}${path.sep}${cam}${path.sep}${quality}${path.sep}${date}`;
        const dir = `${this._publicDir}${path.sep}${descriptorDir}`;

        checkDirectory(dir)
            .then(() => {
                getStatsList(dir, { range: range })
                    .then((statsList) => {
                        if (statsList && statsList.length > 0) {
                            const outputPath = `downloads/${date}`;
                            const name = `${uuidv1()}.ts`;
                            concatVideo(outputPath, name, dir, statsList)
                                .then((descriptor) => {
                                    res.send(this.createResponseData<IGetVideo>(null, {
                                        uuid: uuid,
                                        path: `http://${address}/${outputPath}/${name}`
                                    }));
                                })
                                .catch((reason) => {
                                    res.send(this.createResponseData<IGetDateSequence>({ message: `Concat video error. ${reason}`, code: 720 }, null));
                                });
                            /*createHLSDescriptor(statsList, address, hlsPath, this._publicDir, descriptorDir)
                            .then((descriptor) => {
                                res.send(this.createResponseData<IGetDateSequence>(null, {
                                    uuid: uuid,
                                    date: date,
                                    time: descriptor.range,
                                    descriptor: `${descriptorDir}${path.sep}${descriptor.name}`
                                 }));
                            })
                            .catch((reason) => {
                                res.send(this.createResponseData<IGetDateSequence>({ message: `CreateHLSDescriptor error. ${reason}`, code: 720 }, null));
                            });*/
                        } else {
                            res.send(this.createResponseData<IGetDateSequence>(null, {
                                uuid: uuid,
                                date: date,
                                time: null,
                                descriptor: null
                            }
                            ));
                        }
                    })
                    .catch((reason) => {
                        res.send(this.createResponseData<IGetDateSequence>({ message: `GetStatsList error. ${reason}`, code: 710 }, null));
                    })
            })
            .catch((reason) => {
                res.send(this.createResponseData<IGetDateSequence>({ message: `Date (${date}) for {${uuid}} not found.`, code: 700 }, null));
            })
    }

    private getSequencePath(division: any, cam: string, date: string, quality: string): string {
        return `http://${division.address}/${division.uuid}/${cam}/${quality}/${date}/`;
    }

    private createResponseData<T>(err: IResponseError, doc: T): IResponse<T> {
        return { error: err, data: doc };
    }
}

export interface IGetVideo {
    uuid: string;
    path: string;
}

export interface IGetDateSequence {
    uuid: string;
    date: string;
    time: number[];
    descriptor: string;
}

export interface IResponseError {
    message: string;
    code: number;
}

export interface IResponse<T> {
    error: IResponseError;
    data: T;
}

export const app = new App();