import * as fs from "fs";
import * as path from "path";
import { logger } from "./logger";
const IP = require("ip");

export interface IServerSettings {
    port: string;
}

export interface ICentralServerSettings {
    address: string;
}

export interface IFFMPEGSettings {
    ffmpegPath: string;
    ffprobePath: string;
    flvtoolPath: string;
}

export interface ISettingsData {
    centralServer: ICentralServerSettings;
    server: IServerSettings;
    ffmpeg: IFFMPEGSettings;
}

class MainSettings {

    private _ip: string = "";
    public get ip(): string { return this._ip; }

    private _data: ISettingsData;
    public get data(): ISettingsData { return this._data; }

    public get server(): IServerSettings { return this._data.server; }

    public get ffmpeg(): IFFMPEGSettings { return this._data.ffmpeg; }

    public get centralServer(): ICentralServerSettings { return this._data.centralServer; }

    /**
     * Чтение базовых настроек.
     */
    constructor() {
        this._ip = IP.address();
    }

    public read(cb: (err?: NodeJS.ErrnoException) => void ): void {
        const data = fs.readFileSync('config.json', 'utf8');
        try {
            this._data = JSON.parse(data);
        } catch (e) {
            return cb(e);
        }
        return cb();
    }
}

export let Settings = new MainSettings();
