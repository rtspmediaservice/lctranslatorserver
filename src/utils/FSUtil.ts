import * as express from "express";
import * as path from "path";
import * as fs from "fs";

import * as mkdirRequrs from "mkdir-recursive";
import * as getSize from "get-folder-size";
import { logger } from "../logger";
import { VideoProcessing } from "../videoProcessing";
import * as arraySort from "array-sort";

export const concatVideo = function (outputPath: string, name: string, dir: string, files: IFile[]): Promise<string> {
    return new Promise<string>((resolve, reject) => {
        let videoProcessing = new VideoProcessing();
        videoProcessing.split(outputPath, name, dir, files)
        .then(() => {
            resolve(outputPath);
        })
        .catch((reason) => {
            reject();
        });
    });
}

export interface IMakeDirResult { }

export const mkdirp = function (dir: string): Promise<IMakeDirResult> {
    return new Promise<IMakeDirResult>((resolve, reject) => {
        try {
            mkdirRequrs.mkdir(dir, (err) => {
                if (err) { if (err.message.indexOf('EEXIST') !== -1) resolve(); else reject(err) }
                else resolve();
            });
        } catch (e) {
            console.warn(e)
        }
    });
}

interface IGetFilesSortedByCDateOptions {
    range?: Array<number>;
    reverse?: boolean
}

export interface ICheckDirResult { }

export const checkDirectory = function (directory: string): Promise<ICheckDirResult> {
    return new Promise<ICheckDirResult>((resolve, reject) => {
        fs.stat(directory, function (err, stats) {
            if (err) reject(err);
            else resolve(err)
        });
    });
}

export interface IFile {
    stats: fs.Stats;
    file: string;
}

export const getStatsList = function (dir: string, options?: IGetFilesSortedByCDateOptions): Promise<IFile[]> {
    return new Promise<IFile[]>((resolve, reject) => {
        let files: Array<string>;
        fs.readdir(dir, (err, result) => {
            if (err) {
                reject(err);
                return;
            }
            files = result;
            createStatsList(dir, files)
                .then((statsList) => {
                    if (options && options.range && options.range.length == 2) this.filterStatsByRange(statsList, options.range);
                    arraySort(statsList, 'stats.mtime');
                    resolve(statsList);
                })
                .catch((resolve) => {
                    reject(resolve);
                })
        });
    });
}

export const filterStatsByRange = function (files: IFile[], range: Array<number>): void {
    for (let i = files.length - 1; i >= 0; i--) {
        const f = files[i];
        const timeStart = this.getRelativeDayTime(f.stats.mtime);
        const timeEnd = timeStart + 60;
        if (!(timeStart >= range[0] && timeEnd < range[1])) files.splice(i, 1);
    }
}

export const getRelativeDayTime = function (date: Date): number {
    return date.getHours() * 3600 + date.getMinutes() * 60 + date.getSeconds();
}

/**
 * Создание списка статистики файлов. 
 */
export const createStatsList = function (dir: string, files: string[]): Promise<IFile[]> {
    return new Promise<IFile[]>((resolve, reject) => {
        const stats: IFile[] = [];
        let index: number = 0;
        const nextFile = () => {
            if (files && index < files.length) getStat();
            else resolve(stats);
        }
        const getStat = () => {
            const file = files[index];
            const d = `${dir}${path.sep}${file}`;
            fs.stat(d, (err, s) => {
                if (err) logger.warn(`Error reading stat "${d}"`);
                else if (s.isFile && path.extname(file) === ".ts") stats.push({ file: file, stats: s });
                index ++;
                nextFile();
            })
        }
        nextFile();
    });
}

export interface IHLSDescriptor {
    range: number[];
    name: string;
}

export const createHLSDescriptor = function (stats: IFile[], serverAddress: string, hlsAddress: string, publicDir: string, hlsDir: string): Promise<IHLSDescriptor> {
    return new Promise<IHLSDescriptor>((resolve, reject) => {
        const a = stats[0];
        const b = stats[stats.length - 1];

        let range: number[];

        const aVal = this.timeSeconds(a);
        const bVal = this.timeSeconds(b);
        if (aVal < bVal)
            range = [
                aVal,
                bVal + 60
            ];
        else
            range = [
                bVal,
                aVal + 60
            ];

        const EXTINF: string = '#EXTINF:60.000000,\n';
        let descriptor: string = `#EXTM3U\n#EXT-X-VERSION:3\n#EXT-X-TARGETDURATION:60\n#EXT-X-MEDIA-SEQUENCE:0\n#EXT-X-ALLOW-CACHE:NO\n#EXT-X-PLAYLIST-TYPE:VOD\n`;

        for (const stat of stats) {
            const s = `${EXTINF}${hlsAddress}${stat.file}\n`;
            descriptor += s;
        }

        const descriptorName: string = `${String(a.stats.mtime.getTime())}_${String(b.stats.mtime.getTime() + 60000)}.m3u8`;
        const descriptorDir: string = `${publicDir}${path.sep}${hlsDir}${path.sep}${descriptorName}`;

        this.mkdirp(`${publicDir}${path.sep}${hlsDir}`)
        .then(() => {
            fs.writeFile(descriptorDir, descriptor, (err) => {
                if (err) reject(`Write descriptor error. ${err}`);
                else resolve({ name: descriptorName, range: range });
            });
        })
        .catch((reason) => {
            reject(`mkdir for descriptor error. ${reason}`);
        });
    });
}

export const timeSeconds = function (file: IFile): number {
    return file.stats.mtime.getHours() * 3600 + file.stats.mtime.getMinutes() * 60 + file.stats.mtime.getSeconds();
}

export const getDirectorySize = function (dir: string, callback: Function): void {
    getSize(dir, (err: any, size: number) => {
        if (err) { throw err; }
        if (callback) callback(size);
    });
}

export const getBytesByStr = function (str: string): number {
    let result: number = 0;
    try {
        if (str && str.length > 0) {
            if (str.length > 1 && str.indexOf("K") == str.length - 1 || str.indexOf("k") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000;
            } else if (str.length > 1 && (str.indexOf("M") == str.length - 1 || str.indexOf("m") == str.length - 1)) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000
            } else if (str.length > 1 && str.indexOf("G") == str.length - 1 || str.indexOf("g") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000000;
            } else if (str.length > 1 && str.indexOf("T") == str.length - 1 || str.indexOf("t") == str.length - 1) {
                result = parseFloat(str.substring(0, str.length - 1)) * 1000000000000;
            } else result = parseFloat(str);
        }
    } catch (e) { }
    return result;
}