const dateFormat = require("dateformat");
class Logger {

    constructor() {}
    public formatError(error: any): string {
        if (error) {
            let result = "";
            if (error.message && error.stack) {
                if (error.message)
                    result += error.message + " ";
                if (error.stack)
                    result += "(" + error.stack + ")";
                return result;
            } else
            if (error.error || error.code) {
                if (error.error)
                    result += error.error + " ";
                if (error.code)
                    result += "(" + error.code + ")";
                return result;
            }
            return String(error);
        }
        return "Unknown error";
    }

    private get time(): string {
        return dateFormat(Date.now(), "yyyy-mm-d hh:MM:ss,l");
    }
    public info(message?: any): void {
        console.info(`${this.time} ${message}`);
    }

    public warn(message?: any): void {
        console.warn(`${this.time} ${message}`);
    }

    public error(message?: any): void {
        console.error(`${this.time} ${message}`);
    }

    public req(options: any): void {
        console.info(`${this.time} > ${options.url}${options.body}`);
    }

    public resp(data: string): void {
        console.info(`${this.time} < ${data}`);
    }
}

export const logger = new Logger();