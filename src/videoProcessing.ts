import * as ffmpeg from "fluent-ffmpeg";
import { Settings } from "./settings";
import { logger } from "./logger";
import { IFile, mkdirp } from "./utils/FSUtil";
import * as path from "path";
import { App } from "./app";

export class VideoProcessing {
    private static FF_SIGKILL: string = "SIGKILL";

    private _process: ffmpeg.FfmpegCommand;

    constructor() { }

    split(outputPath: string, name: string, dir: string, files: IFile[]): Promise<string> {
        return new Promise<string>((resolve, reject) => {

            const outputNorm = path.normalize(`${App.instance.publicDir}${path.sep}${outputPath}`);

            mkdirp(outputNorm)
                .then(() => {
                    let sequence: string = 'concat:';
                    let i = 0;
                    for (let file of files) {
                        if (i > 0) sequence += '|';
                        sequence += `${dir}${path.sep}${file.file}`;
                        i++;
                        //if (i === files.length) sequence += '"';
                    }

                    this._process = ffmpeg();
                    this._process
                        .setFfmpegPath(Settings.ffmpeg.ffmpegPath)
                        .setFfprobePath(Settings.ffmpeg.ffprobePath)
                        .addInput(sequence)
                        .on("exit", (code: string, signal: string) => {
                            logger.info(`exit`);
                            reject(new Error(`[ffmpeg] exit. (${code})`));
                            this.dispose();
                        })
                        .on("start", (ffmpegCommand: any) => {
                            logger.info(`Connecting...`)
                            //this.emit(MediaRecordEventTypes.START);
                        })
                        .on("progress", (data: any) => {
                            logger.info(`concat ${data.frames}`);
                            //this.emit(MediaRecordEventTypes.PROGRESS);
                        })
                        .on("end", () => {
                            logger.info(`End.`);
                            resolve();
                            this.dispose();
                            //this.emit(MediaRecordEventTypes.FAIL);
                            //this.emit(MediaRecordEventTypes.COMPLETE);
                        })
                        .on("error", (error: any) => {
                            logger.error(`Error on write stream. ${error.message}`);
                            reject();
                            this.dispose();
                            //this.emit(MediaRecordEventTypes.FAIL);
                        })
                        .outputOptions([
                            "-vcodec copy",
                            "-an"
                        ])
                        .output(`${outputNorm}${path.sep}${name}`)
                        .run();
                })
                .catch((reason) => {
                    logger.info(`#concat::#split::mkdir error. ${reason}`)
                });
        });
    }

    public dispose(): void {
        if (this._process) {
            this._process.kill(VideoProcessing.FF_SIGKILL);
            this._process = null;
        }
    }
}